import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
numA;
numB;
ans=0;
  constructor(public navCtrl: NavController) {

  }
add(){
  var a=parseInt(this.numA);
  var b=parseInt(this.numB);
  
  this.ans=a+b;
}
sub(){
  var a=parseInt(this.numA);
  var b=parseInt(this.numB);
  this.ans=a-b;
}
mul(){
  var a=parseInt(this.numA);
  var b=parseInt(this.numB);
  this.ans=a*b;
}
div(){
  var a=parseInt(this.numA);
  var b=parseInt(this.numB);
  if(b==0){
    alert("Not Possible");
  }else{
    this.ans=a/b;}
}
}
